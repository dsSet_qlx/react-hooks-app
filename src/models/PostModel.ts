

export interface PostModel {

  title: string;

  content: string;

  author: string;

  isPublished: boolean;

}


export const validateModel = (key: keyof PostModel, value: any): string | null => {
  if (key === 'isPublished') {
    return null;
  }

  return Boolean(value) ? null : 'Field is mandatory';
};
