import React from 'react';
import classNames from 'classnames';

export const PostForm: React.FC<React.FormHTMLAttributes<HTMLFormElement>> = ({ children, className, ...props}) => {
  return (
    <form {...props} className={classNames(className, 'mt-5')}>
      {children}
    </form>
  )
};
