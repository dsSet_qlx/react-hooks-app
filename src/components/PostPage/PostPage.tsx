import React, { SyntheticEvent, useCallback, useEffect } from 'react';
import { PostForm } from './PostForm';
import { Error, FormActions, FormControl, FormHeader, Input, Label, Textarea } from '../form';
import { Checkbox } from '../form/Checkbox';
import { Button } from '../button';
import { usePostForm } from '../../hooks/usePostForm';
import { useCaptcha } from '../../hooks/useCaptcha';
import { PostModel } from '../../models/PostModel';
import { usePostApi } from '../../hooks/usePostApi';

export const PostPage: React.FC = () => {
  const {
    state,
    changeTitle,
    changeAuthor,
    changeDescription,
    changeIsPublished,
    cleanUp,
    getFieldError,
    validate,
    setModel
  } = usePostForm();
  const { getPost, savePost, isLoading } = usePostApi(setModel, cleanUp, console.error);
  const { execute, renderRecaptcha } = useCaptcha<[PostModel]>(savePost, console.error);

  useEffect(() => { getPost(); }, []);

  const handleSubmit = useCallback((e: SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (validate(state)) {
      execute(state);
    }
  }, [execute, state, validate]);

  return (
    <PostForm onSubmit={handleSubmit}>
      <FormHeader>
        Post your content!
      </FormHeader>

      <FormControl>
        <Label>Post title</Label>
        <Input onChange={changeTitle} value={state.title} />
        <Error>{getFieldError('title')}</Error>
      </FormControl>

      <FormControl>
        <Label>Post description</Label>
        <Textarea onChange={changeDescription} value={state.content} />
        <Error>{getFieldError('content')}</Error>
      </FormControl>

      <FormControl>
        <Label>Author</Label>
        <Input onChange={changeAuthor} value={state.author} />
        <Error>{getFieldError('author')}</Error>
      </FormControl>

      <Checkbox onChange={changeIsPublished} checked={state.isPublished}>
        Publish
      </Checkbox>

      {renderRecaptcha()}

      <FormActions>
        <Button outline disabled={isLoading} onClick={getPost} type="button">Load</Button>
        <Button outline disabled={isLoading} onClick={cleanUp} type="reset" >Reset</Button>
        <Button disabled={isLoading} type="submit">Submit</Button>
      </FormActions>

    </PostForm>
  );
};
