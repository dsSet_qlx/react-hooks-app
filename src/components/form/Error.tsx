import React from 'react';
import classNames from 'classnames';

export const Error: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({ children, className, ...props}) => {
  return (
    <div {...props} className={classNames('text-danger', className)}>
      {children ? children : <>&nbsp;</>}
    </div>
  );
};
