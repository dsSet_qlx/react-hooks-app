import React from 'react';
import classNames from 'classnames';

export const Checkbox: React.FC<React.InputHTMLAttributes<HTMLInputElement>> = ({ children, className, ...props}) => {
  return (
    <label className={classNames(className, 'form-check')}>
      <input {...props} className="form-check-input" type="checkbox" />
      {children}
    </label>
  );
};
