import React from 'react';
import classNames from 'classnames';
import { useConfig } from '../../hooks/useConfig';

export const Textarea: React.FC<React.TextareaHTMLAttributes<HTMLTextAreaElement>> = ({ className, ...props}) => {
  const { colorScheme } = useConfig();
  return (
    <textarea {...props} className={classNames('form-control', `border-${colorScheme}`, className)} />
  );
};
