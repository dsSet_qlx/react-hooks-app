import React from 'react';

export const FormControl: React.FC = ({ children }) => {
  return (
    <div className="mb-3">
      {children}
    </div>
  )
};
