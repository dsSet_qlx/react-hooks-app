import React from 'react';
import classNames from 'classnames';

export const FormActions: React.FC = ({ children }) => {
  return (
    <div className={classNames('d-flex', 'flex-row-reverse', 'mt-3')}>
      {children}
    </div>
  )
};
