import React from 'react';
import classNames from 'classnames';
import { useConfig } from '../../hooks/useConfig';

export const Input: React.FC<React.InputHTMLAttributes<HTMLInputElement>> = ({ className, ...props}) => {
  const { colorScheme } = useConfig();
  return (
    <input {...props} className={classNames(
      'form-control',
      `border-${colorScheme}`,
      className
    )} />
  );
};
