import React from 'react';

export const FormHeader: React.FC = ({ children }) => {
  return (
    <h2 className="mb-3 align-text-bottom">
      {children}
    </h2>
  )
};
