import React from 'react';
import classNames from 'classnames';

export const Label: React.FC<React.LabelHTMLAttributes<HTMLLabelElement>> = ({ children, className, ...props}) => {
  return (
    <label {...props} className={classNames('form-label', className)}>
      <i className="icon" />
      {children}
    </label>
  )
};
