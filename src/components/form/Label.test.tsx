import React from 'react';
import { mount, render, shallow } from 'enzyme';
import { Label } from './Label';

describe('<Label />', () => {
  it('should have default render', () => {
    const wrapper = shallow(<Label />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should have render with props', () => {
    const wrapper = shallow(<Label className="mock-label" htmlFor="unknown-input" />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should have real dom', () => {
    const wrapper = render(<Label htmlFor="unknown-input" />);
    expect(wrapper).toMatchSnapshot();
  });
});
