import React, { useCallback } from 'react';
import { ColorScheme } from '../../config';
import classNames from 'classnames';

export interface SchemeItemProps {
  value: ColorScheme;
  active: boolean;
  onClick: (value: ColorScheme) => void;
}

export const SchemeItem: React.FC<SchemeItemProps> = ({ value, active, children, onClick}) => {
  const handleClick = useCallback(() => {
    onClick(value);
  }, [onClick, value]);

  return (
    <button onClick={handleClick} className={classNames('btn', 'btn-sm', {
      [`btn-${value}`]: active,
      [`btn-outline-${value}`]: !active
    })}>
      {children}
    </button>
  )
};
