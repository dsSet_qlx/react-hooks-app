import React from 'react';
import { ColorScheme } from '../../config';
import { SchemeItem } from './SchemeItem';

interface ThemePickerProps {
  onSetTheme: (schema: ColorScheme) => void;
  scheme: ColorScheme;
}

export const ThemePicker: React.FC<ThemePickerProps> = ({ onSetTheme, scheme }) => {
  return (
    <div>
      <SchemeItem value="info" active={scheme === 'info'} onClick={onSetTheme}>
        Red
      </SchemeItem>
      <SchemeItem value="primary" active={scheme === 'primary'} onClick={onSetTheme}>
        Blue
      </SchemeItem>
      <SchemeItem value="secondary" active={scheme === 'secondary'} onClick={onSetTheme}>
        Gray
      </SchemeItem>
    </div>
  )
};
