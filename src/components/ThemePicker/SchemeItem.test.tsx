import React from 'react';
import { shallow } from 'enzyme';
import { SchemeItem, SchemeItemProps } from './SchemeItem';

const props: React.PropsWithChildren<SchemeItemProps> = {
  active: false,
  value: 'primary',
  onClick: jest.fn(),
  children: (<p>mock children</p>)
};

describe('<SchemeItem />', () => {
  describe('should have snapshots', () => {
    it('should have default render', () => {
      const wrapper = shallow(<SchemeItem {...props} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('should have render with active state', () => {
      const wrapper = shallow(<SchemeItem {...props} active />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('should handle actions', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

     it('should handle click with onClick function', () => {
       const wrapper = shallow(<SchemeItem {...props} />);
       wrapper.simulate('click');
       expect(props.onClick).toHaveBeenCalledWith(props.value);
     });
  });
});
