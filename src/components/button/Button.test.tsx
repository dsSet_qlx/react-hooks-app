import React from 'react';
import { useConfig } from '../../hooks/useConfig';
import { Button, ButtonProps } from './Button';
import { shallow } from 'enzyme';
import { ColorScheme } from '../../config';
import { mocked } from 'ts-jest/utils';

jest.mock('../../hooks/useConfig');

const colorScheme: ColorScheme = 'info';

mocked(useConfig).mockImplementation(() => ({ colorScheme, recaptchaId: '' }));

describe('<Button />', () => {

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should have render", () => {
    const props: ButtonProps = {
      outline: false,
      customClick: jest.fn()
    };
    const wrapper = shallow(<Button {...props} />);
    expect(wrapper).toMatchSnapshot();
    expect(useConfig).toHaveBeenCalled();
  });

  it('should call customClick on button click', () => {
    const customClick = jest.fn();
    const wrapper = shallow(<Button customClick={customClick} />);
    const event = { data: 'click event' };
    const button = wrapper.findWhere(node => node.key() === 'button');
    button.simulate('click', event);
    expect(customClick).toHaveBeenCalledWith(event);
  });
});
