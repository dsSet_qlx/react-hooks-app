import React from 'react';
import classNames from 'classnames';
import { useConfig } from '../../hooks/useConfig';

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  outline?: boolean;
  customClick?: () => void;
}

export const Button: React.FC<ButtonProps> = ({ children, outline, customClick, className, ...props}) => {
  const { colorScheme } = useConfig();
  return (
    <>
      <button
        key="button"
        {...props}
        className={classNames(className, 'btn', {
          [`btn-${colorScheme}`]: !outline,
          [`btn-outline-${colorScheme}`]: outline
        })}
        onClick={customClick}
      >
        {children}
      </button>
    </>
  )
};
