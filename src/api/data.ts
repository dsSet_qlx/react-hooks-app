import { PostModel } from '../models/PostModel';
import { postModel } from './stubs';

const delay = (timeout: number): Promise<void> => new Promise(resolve => { setTimeout(resolve, timeout); });

export const getFormData = async (): Promise<PostModel> => {
  await delay(1000);
  return Object.assign({}, postModel);
};

export const saveFormData = async (data: PostModel & { token: string | null }): Promise<boolean> => {
  await delay(1000);
  console.log('Mock API: Post model saved!', data);
  return true;
};
