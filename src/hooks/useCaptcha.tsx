import React, { useCallback, useRef } from 'react';
import ReCaptcha, { ReCAPTCHA, ReCAPTCHAProps } from 'react-google-recaptcha';
import { useConfig } from './useConfig';

interface CaptchaInterface<TArgs extends Array<any>> {
  execute: (...args: TArgs) => void;
  renderRecaptcha: (options?: Partial<ReCAPTCHAProps>) => any;
}

export const useCaptcha = <TArgs extends Array<any>>(
  onSuccess?: (token: string | null, ...args: TArgs) => void,
  onError?: (message: string) => void
): CaptchaInterface<TArgs> => {
  const recaptcha = useRef<ReCAPTCHA | null>(null);

  const delegatedArgs = useRef<Array<any>>([]);

  const { recaptchaId } = useConfig();

  const handleExpired = useCallback(() => {
    recaptcha.current?.reset();
  }, []);

  const handleError = useCallback(() => {
    if (onError) {
      onError('recaptcha unavailable');
    }
  }, [onError]);

  const handleSuccess = useCallback((token: string | null) => {
    if (onSuccess) {
      onSuccess(token, ...delegatedArgs.current as TArgs)
    }
    recaptcha.current?.reset();
  }, [onSuccess]);

  const execute = useCallback((...args: TArgs) => {
    delegatedArgs.current = args;
    recaptcha.current?.execute();
  }, []);

  const renderRecaptcha = useCallback((options: Partial<ReCAPTCHAProps> = { badge: 'bottomright', size: 'invisible' }) => {
    return (
      <ReCaptcha
        {...options}
        onExpired={handleExpired}
        onErrored={handleError}
        onChange={handleSuccess}
        sitekey={recaptchaId}
        key="recaptcha"
        ref={recaptcha}
    />
    )
  }, [handleExpired, handleError, handleSuccess, recaptchaId]);

  return {
    execute,
    renderRecaptcha,
  };
};
