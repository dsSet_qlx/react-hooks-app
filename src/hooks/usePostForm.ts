import React, { useCallback, useReducer } from 'react';
import { PostModel, validateModel } from '../models/PostModel';
import { useValidation } from './useValidation';


interface Action {
  type: string;
  payload: unknown;
}

const initialState: PostModel = {
  isPublished: false,
  content: '',
  title: '',
  author: ''
};

const reducer = (state: PostModel, action: Action): PostModel => {
  const { type, payload } = action;
  switch (type) {
    case 'CHANGE_FIELD':
    {
      const [key, value] = payload as [keyof PostModel, string | boolean];
      return {
        ...state,
        [key]: value
      };
    }
    case 'RESET':
      return payload ? (payload as PostModel) : { ...initialState };
    default: return state;
  }
};

export const usePostForm = () => {
  const { validateField, getFieldError, validate, reset: resetValidation } = useValidation<PostModel>(validateModel);

  const [state, dispatch] = useReducer(reducer, initialState);

  const changeField = useCallback((fieldName: keyof PostModel, event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { target } = event;
    validateField(fieldName, target.value);
    dispatch({ type: 'CHANGE_FIELD', payload: [fieldName, target.value] } as Action)
  }, [dispatch, validateField]);

  const changeBoolField = useCallback((fieldName: keyof PostModel, event: React.ChangeEvent<HTMLInputElement>) => {
    const { target } = event;
    dispatch({ type: 'CHANGE_FIELD', payload: [fieldName, target.checked] } as Action)
  }, [dispatch]);

  const changeTitle = useCallback((event: React.ChangeEvent<HTMLInputElement>) => changeField('title', event), [changeField]);
  const changeDescription = useCallback((event: React.ChangeEvent<HTMLTextAreaElement>) => changeField('content', event), [changeField]);
  const changeAuthor = useCallback((event: React.ChangeEvent<HTMLInputElement>) => changeField('author', event), [changeField]);
  const changeIsPublished = useCallback((event: React.ChangeEvent<HTMLInputElement>) => changeBoolField('isPublished', event), [changeBoolField]);
  const setModel = useCallback((model?: PostModel) => {
    resetValidation();
    dispatch({ type: 'RESET', payload: model } as Action);
  }, [resetValidation]);
  const cleanUp = useCallback(() => {
    setModel();
  }, [setModel]);

  return {
    changeField,
    changeTitle,
    changeDescription,
    changeAuthor,
    changeIsPublished,
    state,
    cleanUp,
    validate,
    setModel,
    getFieldError,
  }
};
