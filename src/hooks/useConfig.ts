import React, { useCallback, useContext, useMemo, useReducer } from 'react';
import { AppConfig, ColorScheme, config } from '../config';

interface Action {
  type: string;
  payload: unknown;
}

export interface ConfigContextData {
  state: AppConfig;
  setColorScheme: (scheme: ColorScheme) => void;
}

export const defaultContext: ConfigContextData = {
  state: { ...config },
  setColorScheme: (scheme: ColorScheme) => {},
};

const reducer = (state: AppConfig, action: Action): AppConfig => {
  const { type, payload } = action;
  switch (type) {
    case 'SET_COLOR_SCHEME':
      return {
        ...state,
        colorScheme: payload as ColorScheme
      };
    default:
      return state;
  }
};

export const ConfigContext = React.createContext<ConfigContextData>(defaultContext);

export const useConfigContext = (): ConfigContextData => {
  const [state, dispatch] = useReducer(reducer, config);
  const setColorScheme = useCallback((scheme: ColorScheme) => {
    dispatch({ type: 'SET_COLOR_SCHEME', payload: scheme });
  }, [dispatch]);
  return useMemo(() => ({
    state,
    setColorScheme
  }), [state, setColorScheme]);
};

export const useConfig = (): AppConfig => {
  const { state } = useContext(ConfigContext);
  return state;
};
