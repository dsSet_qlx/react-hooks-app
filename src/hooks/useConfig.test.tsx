import React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { ConfigContext, ConfigContextData, defaultContext, useConfig } from './useConfig';

describe('hooks/useConfig', () => {
  describe('useConfig', () => {
    it('should return default config', () => {
      const { result } = renderHook(() => useConfig());
      expect(result.current).toEqual(defaultContext.state);
      // const config = useConfig();
      // expect(config).toEqual(defaultContext.state);
    });

    it('should return config from context', () => {
      const context: ConfigContextData = {
        state: {
          colorScheme: 'secondary',
          recaptchaId: 'OVERLOADED RECAPTCHA KEY'
        },
        setColorScheme: jest.fn()
      };
      const wrapper: React.FC = (props) => (<ConfigContext.Provider value={context} {...props} />);

      const { result } = renderHook(() => useConfig(), { wrapper });
      expect(result.current).toEqual(context.state);
    });
  });
});
