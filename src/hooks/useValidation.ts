import { useCallback, useState } from 'react';

export const useValidation = <TModel extends Record<string, any>>(validate: (name: keyof TModel, value: any) => string | null) => {
  const [errors, setErrors] = useState<Partial<Record<keyof TModel, string | null>>>({});

  const validateField = useCallback((name: keyof TModel, value: any) => {
    const result = validate(name, value);
    setErrors((errors) =>
      result === errors[name] ? errors : { ...errors, [name]: result }
    );
  }, [validate, setErrors]);

  const isValid = useCallback((errors: Partial<Record<keyof TModel, string | null>>): boolean => {
    return !Object.values(errors).find((value: any) => Boolean(value));
  }, []);

  const validateModel = useCallback((model: TModel) => {
    const validationResult: Partial<Record<keyof TModel, string | null>> = {};
    Object.keys(model).forEach((key: keyof TModel) => {
      validationResult[key] = validate(key, model[key]);
    });
    setErrors(validationResult);
    return isValid(validationResult);
  }, [validate, setErrors, isValid]);

  const getFieldError = useCallback((name: keyof TModel) => {
    return errors[name] || null;
  }, [errors]);

  const reset = useCallback(() => {
    setErrors({});
  }, [setErrors]);

  return {
    reset,
    validate: validateModel,
    validateField,
    getFieldError,
  };
};
