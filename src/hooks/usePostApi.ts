import { Error } from '../components/form';
import { useCallback, useState } from 'react';
import { PostModel } from '../models/PostModel';
import { getFormData, saveFormData } from '../api/data';

const useSavePost = (onSuccess?: () => void, onError?: (error: Error) => void) => {
  const [isLoading, setIsLoading] = useState(false);

  const savePost = useCallback(async (token: string | null, data: PostModel) => {
    try {
      setIsLoading(true);
      await saveFormData({ ...data, token });
    } catch (e) {
      if (onError) {
        onError(e);
      }
    } finally {
      setIsLoading(false);
    }

    if (onSuccess) {
      onSuccess();
    }
  }, [onSuccess, onError]);


  return {
    savePost,
    isLoading
  }
};

const useLoadPost = (onSuccess?: (model: PostModel) => void, onError?: (error: Error) => void) => {
  const [isLoading, setIsLoading] = useState(false);

  const getPost = useCallback(async () => {
    try {
      setIsLoading(true);
      const data = await getFormData();
      if (onSuccess) {
        onSuccess(data);
      }
    } catch (e) {
      if (onError) {
        onError(e);
      }
    } finally {
      setIsLoading(false);
    }
  }, [onSuccess, onError]);


  return {
    getPost,
    isLoading
  }
};

export const usePostApi = (
  onPostLoad?: (model: PostModel) => void,
  onPostSave?: () => void,
  onError?: (error: Error) => void
) => {
  const { isLoading: savePending, savePost } = useSavePost(onPostSave, onError);
  const { isLoading: loadPending, getPost } = useLoadPost(onPostLoad, onError);

  return {
    savePost,
    getPost,
    isLoading: savePending || loadPending
  }
};
