
export type ColorScheme = 'primary' | 'secondary' | 'info';

export interface AppConfig {
  recaptchaId: string;
  colorScheme: ColorScheme;
}

export const config: AppConfig = {
  recaptchaId: '6LfUhkMUAAAAALkzhSxmnDNvTX1TfWcJ29J1-5eJ',
  colorScheme: 'primary'
};
