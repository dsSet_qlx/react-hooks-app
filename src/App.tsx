import React from 'react';
import { PostPage } from './components/PostPage';
import { useConfigContext, ConfigContext } from './hooks/useConfig';
import { ThemePicker } from './components/ThemePicker';

import './App.scss';


function App() {
  const configContext = useConfigContext();
  const { setColorScheme, state } = configContext;

  return (
    <ConfigContext.Provider value={configContext}>
      <div className="container container-fluid">
        <header>
          <ThemePicker onSetTheme={setColorScheme} scheme={state.colorScheme} />
        </header>
        <main>
          <PostPage />
        </main>
      </div>
    </ConfigContext.Provider>
  );
}

export default App;
